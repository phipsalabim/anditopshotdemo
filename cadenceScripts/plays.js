const constants = require('../constants')
const fcl = require('@onflow/fcl')

const getAllPlays = fcl.script`
    import TopShot from 0x${constants.topshotAddress}
    pub fun main(): [TopShot.Play] {
      return TopShot.getAllPlays()
    }
`

module.exports = {
  getAllPlays
}

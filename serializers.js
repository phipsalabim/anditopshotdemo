const converter = require('json-2-csv')

const playSerializer = (plays) => {
  const flattenedPlays = plays.map(play => ({ playID: play.playID, ...play.metadata }))
  return converter.json2csvAsync(flattenedPlays)
}

module.exports = {
  playSerializer
}

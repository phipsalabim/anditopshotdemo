const fcl = require('@onflow/fcl')
const constants = require('./constants')
const playScripts = require('./cadenceScripts/plays')
const serializers = require('./serializers')
const utils = require('./utils')


fcl.config()
  .put("accessNode.api", constants.accessNode)

fcl.send([
    playScripts.getAllPlays
  ])
  .then(fcl.decode)
  .then(serializers.playSerializer)
  .then(csvString => utils.writeToFile('plays.csv', csvString))
  .then(fname => console.log(`successfully written data to file: ${fname}`))
  .catch(err => console.log(err))

const fs = require('fs').promises

const writeToFile = async (filename, data) => {
  try {
    await fs.writeFile(filename, data)
    return Promise.resolve(filename)
  } catch (error) {
    return Promise.reject(error)
  }
}

module.exports = {
  writeToFile
}
